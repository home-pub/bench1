package hello.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(indexes = {
        @Index(name = "idx_name", columnList = "lastName,firstName")
})
public class Person {

    @Id
    @TableGenerator(name = "pGen", pkColumnValue = "pId")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "pGen")
    private long   id;
    @NotBlank(message = "{firstName.cannot.be.blank}")
    @Column(length = 255, nullable = false)
    @Length(min = 3, max = 255, message = "{firstName.length}")
    private String firstName;
    @NotBlank(message = "{lastName.cannot.be.blank}")
    @Column(length = 255, nullable = false)
    @Length(min = 3, max = 255, message = "{lastName.length}")
    private String lastName;

    public Person() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
